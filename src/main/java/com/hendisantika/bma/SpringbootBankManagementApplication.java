package com.hendisantika.bma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootBankManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootBankManagementApplication.class, args);
    }

}
