package com.hendisantika.bma.repository;

import com.hendisantika.bma.entity.Log;
import com.hendisantika.bma.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bank-management-application
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-03
 * Time: 18:16
 */
@Repository
public interface LogRepository extends JpaRepository<Log, Long> {
    Log findByUser(User user);

    List<Log> findByUserAndTimestampBetween(User user, Date from, Date to);
}
