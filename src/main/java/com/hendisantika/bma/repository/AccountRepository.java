package com.hendisantika.bma.repository;

import com.hendisantika.bma.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bank-management-application
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-03
 * Time: 13:41
 */
@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
}