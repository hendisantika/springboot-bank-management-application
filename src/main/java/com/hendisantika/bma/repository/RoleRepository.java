package com.hendisantika.bma.repository;

import com.hendisantika.bma.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bank-management-application
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-03
 * Time: 18:17
 */
public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByRole(String role);
}