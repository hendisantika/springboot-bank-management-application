package com.hendisantika.bma.repository;

import com.hendisantika.bma.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bank-management-application
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-03
 * Time: 13:41
 */
public interface ClientRepository extends JpaRepository<Client, Long> {
    Client findByName(String name);
}
