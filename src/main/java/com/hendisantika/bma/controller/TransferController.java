package com.hendisantika.bma.controller;

import com.hendisantika.bma.entity.Account;
import com.hendisantika.bma.entity.Log;
import com.hendisantika.bma.entity.User;
import com.hendisantika.bma.repository.AccountRepository;
import com.hendisantika.bma.repository.LogRepository;
import com.hendisantika.bma.repository.UserRepository;
import com.hendisantika.bma.validator.TransactionValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bank-management-application
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-18
 * Time: 09:25
 */
@Controller
@RequestMapping("/transfer")
public class TransferController {
    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private UserRepository userRepository;

    private TransactionValidator validator;

    @GetMapping
    public String transfer() {
        return "transfer";
    }

    @GetMapping("/search")
    public String search(@RequestParam(value = "id1") long id1, @RequestParam(value = "id2") long id2, Model model) {
        Account from = accountRepository.getOne(id1);
        Account to = accountRepository.getOne(id2);
        model.addAttribute("from", from);
        model.addAttribute("to", to);
        return "transfer/search";
    }

    @PostMapping("/process")
    public String processTransaction(HttpServletRequest request) {
        Double amount = Double.parseDouble(request.getParameter("amount"));
        Account from = accountRepository.getOne(Long.parseLong(request.getParameter("id1")));
        Account to = accountRepository.getOne(Long.parseLong(request.getParameter("id2")));

        validator = new TransactionValidator();
        if (validator.validate(from, amount)) {
            from.setBalance(from.getBalance() - amount);
            to.setBalance(to.getBalance() + amount);

            accountRepository.save(from);
            accountRepository.save(to);
        } else
            return "redirect:/transfer?error=true";


        Log log = new Log();
        log.setOperation("Transaction: From " + from.getId() + " To " + to.getId() + " Amount " + amount);
        log.setTimestamp(new Timestamp(System.currentTimeMillis()));

        String username = request.getRemoteUser();

        User user = userRepository.findByUsername(username);
        log.setUser(user);

        logRepository.save(log);

        return "redirect:/index";
    }
}
