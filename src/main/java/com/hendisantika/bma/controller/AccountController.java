package com.hendisantika.bma.controller;

import com.hendisantika.bma.entity.Account;
import com.hendisantika.bma.entity.Client;
import com.hendisantika.bma.entity.Log;
import com.hendisantika.bma.entity.User;
import com.hendisantika.bma.repository.AccountRepository;
import com.hendisantika.bma.repository.ClientRepository;
import com.hendisantika.bma.repository.LogRepository;
import com.hendisantika.bma.repository.UserRepository;
import com.hendisantika.bma.validator.AccountValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bank-management-application
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-17
 * Time: 18:34
 */
@Controller
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private UserRepository userRepository;

    private AccountValidator validator;

    @GetMapping("/new")
    public String newAccount() {
        return "account/new";
    }

    @PostMapping("/new")
    public String addAccount(HttpServletRequest request) {
        Account account = new Account();
        account.setBalance(Double.parseDouble(request.getParameter("balance")));
        account.setDate(new Date(System.currentTimeMillis()));
        account.setType(request.getParameter("type"));
        validator = new AccountValidator();
        Client client = clientRepository.findByName(request.getParameter("name"));
        if (client != null)
            account.setOwner(client);
        else
            return "account/new?error=true";
        if (validator.validate(account))
            accountRepository.save(account);
        else
            return "account/new?error=true";

        Log log = new Log();
        log.setOperation("Account added. ID: " + account.getId() + "; Owner: " + account.getOwner().getName());
        log.setTimestamp(new Timestamp(System.currentTimeMillis()));

        String username = request.getRemoteUser();

        User user = userRepository.findByUsername(username);
        log.setUser(user);

        logRepository.save(log);

        return "redirect:/index";
    }

    @GetMapping("/{id}/edit")
    public String update(@PathVariable long id, Model model) {
        Optional<Account> account = accountRepository.findById(id);
        Optional<Client> client = clientRepository.findById(account.get().getOwner().getId());
        model.addAttribute("owner", client);
        model.addAttribute("account", account);
        return "account/edit";
    }

    @PostMapping("/update")
    public String updateAccount(HttpServletRequest request) {
        Optional<Account> account1 = accountRepository.findById(Long.parseLong(request.getParameter("id")));
        Account account = account1.get();
        account.setBalance(Double.parseDouble(request.getParameter("balance")));
        account.setType(request.getParameter("type"));

        Optional<Client> client = clientRepository.findById(account.getOwner().getId());
        account.setOwner(client.get());
        if (client != null)
            account.setOwner(client.get());
        else
            return "account/new?error=true";
        if (validator.validate(account))
            accountRepository.save(account);
        else
            return "account/new?error=true";
        accountRepository.save(account);
        Log log = new Log();
        log.setOperation("Account updated. ID: " + account.getId() + "; Owner: " + account.getOwner().getName());
        log.setTimestamp(new Timestamp(System.currentTimeMillis()));

        String username = request.getRemoteUser();

        User user = userRepository.findByUsername(username);
        log.setUser(user);

        logRepository.save(log);

        return "redirect:/index";
    }

    @GetMapping("/{id}/view")
    public String view(@PathVariable long id, Model model) {
        Optional<Account> account = accountRepository.findById(id);
        Client client = account.get().getOwner();
        model.addAttribute("client", client);
        model.addAttribute("account", account);
        return "account/view";
    }

    @GetMapping("/{id}/delete")
    public String delete(@PathVariable long id) {
        accountRepository.deleteById(id);
        return "redirect:/index";
    }

    @GetMapping("/search")
    public String displaySearch() {
        return "account/search";
    }

    @PostMapping("/search")
    public String search(HttpServletRequest request) {
        Account account = accountRepository.findById(Long.parseLong(request.getParameter("id"))).get();
        String option = request.getParameter("option");
        if (option.equals("VIEW")) {
            return "redirect:/account/" + account.getId() + "/view";
        }
        if (option.equals("EDIT")) {
            return "redirect:/account/" + account.getId() + "/edit";
        }
        if (option.equals("DELETE")) {
            return "redirect:/account/" + account.getId() + "/delete";
        }
        return "redirect:/account/search";
    }

}
