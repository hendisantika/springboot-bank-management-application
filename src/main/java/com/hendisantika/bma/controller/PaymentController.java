package com.hendisantika.bma.controller;

import com.hendisantika.bma.entity.Account;
import com.hendisantika.bma.entity.Log;
import com.hendisantika.bma.entity.User;
import com.hendisantika.bma.repository.AccountRepository;
import com.hendisantika.bma.repository.LogRepository;
import com.hendisantika.bma.repository.UserRepository;
import com.hendisantika.bma.validator.TransactionValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bank-management-application
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-20
 * Time: 08:12
 */
@Controller
@RequestMapping("/payment")
public class PaymentController {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private UserRepository userRepository;

    private TransactionValidator validator;

    @GetMapping
    public String show() {
        return "payment";
    }

    @PostMapping
    public String process(HttpServletRequest request) {
        Account account = accountRepository.getOne(Long.parseLong(request.getParameter("id")));
        Double amount = Double.parseDouble(request.getParameter("amount"));
        String type = request.getParameter("bill");
        validator = new TransactionValidator();
        if (validator.validate(account, amount)) {
            account.setBalance(account.getBalance() - amount);
            accountRepository.save(account);
        } else
            return "redirect:/payment?error=true";


        Log log = new Log();
        log.setOperation("Utilities payment: Account ID: " + account.getId() + " Amount: " + amount + " Type: " + type);
        log.setTimestamp(new Timestamp(System.currentTimeMillis()));

        String username = request.getRemoteUser();

        User user = userRepository.findByUsername(username);
        log.setUser(user);

        logRepository.save(log);

        return "redirect:/index";
    }
}
