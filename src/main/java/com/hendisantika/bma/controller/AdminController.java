package com.hendisantika.bma.controller;

import com.hendisantika.bma.entity.Employee;
import com.hendisantika.bma.entity.Log;
import com.hendisantika.bma.entity.Role;
import com.hendisantika.bma.entity.User;
import com.hendisantika.bma.repository.EmployeeRepository;
import com.hendisantika.bma.repository.LogRepository;
import com.hendisantika.bma.repository.RoleRepository;
import com.hendisantika.bma.repository.UserRepository;
import com.hendisantika.bma.validator.EmployeeValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bank-management-application
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-17
 * Time: 18:53
 */
@Controller
@RequestMapping(value = "/admin")
public class AdminController {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private LogRepository logRepository;

    private EmployeeValidator validator;

    @GetMapping("/new")
    public String showForm(ModelAndView modelAndView) {
        return "admin/new";
    }


    @PostMapping("/add")
    public String registerUser(HttpServletRequest request) {
        List<Employee> employees = employeeRepository.findAll();


        Employee employee = new Employee();
        String salary = request.getParameter("salary");

        employee.setSalary(Double.parseDouble(salary));
        employee.setName(request.getParameter("name"));
        employee.setPhone(request.getParameter("phone"));
        employee.setEmail(request.getParameter("email"));
        validator = new EmployeeValidator();
        if (validator.validate(employee))
            employeeRepository.save(employee);
        else
            return "redirect:/admin/new?error=true";

        User user = new User();
        user.setUsername(request.getParameter("username"));
        user.setPassword(request.getParameter("password"));

        Role role = roleRepository.findByRole(request.getParameter("role"));
        user.setRoles(new HashSet<Role>(Arrays.asList(role)));
        userRepository.save(user);

        Log log = new Log();
        log.setOperation("Employee added: ID: " + employee.getId());
        log.setTimestamp(new Timestamp(System.currentTimeMillis()));

        String username = request.getRemoteUser();

        User activeUser = userRepository.findByUsername(username);
        log.setUser(activeUser);

        logRepository.save(log);
        return "redirect:/index";
    }

    @GetMapping("/{id}/edit")
    public String update(@PathVariable long id, Model model) {
        Optional<Employee> employee = employeeRepository.findById(id);
        model.addAttribute("employee", employee.get());
        return "admin/edit";
    }

    @PostMapping("/update")
    public String updateEmployee(HttpServletRequest request) {
        Employee employee = employeeRepository.findById(Long.parseLong(request.getParameter("id"))).get();
        employee.setSalary(Double.parseDouble(request.getParameter("salary")));
        employee.setEmail(request.getParameter("email"));
        employee.setName(request.getParameter("name"));
        employee.setPhone(request.getParameter("phone"));


        validator = new EmployeeValidator();
        if (validator.validate(employee))
            employeeRepository.save(employee);
        else
            return "redirect:/admin/new?error=true";
        Log log = new Log();
        log.setOperation("Employee updated. ID: " + employee.getId());
        log.setTimestamp(new Timestamp(System.currentTimeMillis()));

        String username = request.getRemoteUser();

        User user = userRepository.findByUsername(username);
        log.setUser(user);

        logRepository.save(log);

        return "redirect:/index";
    }

    @GetMapping("/search")
    public String displaySearch() {
        return "admin/search";
    }

    @PostMapping("/search")
    public String search(HttpServletRequest request) {
        Employee employee = employeeRepository.findById(Long.parseLong(request.getParameter("id"))).get();
        String option = request.getParameter("option");
        if (option.equals("VIEW")) {
            return "redirect:/admin/" + employee.getId() + "/view";
        }
        if (option.equals("EDIT")) {
            return "redirect:/admin/" + employee.getId() + "/edit";
        }
        if (option.equals("DELETE")) {
            return "redirect:/admin/" + employee.getId() + "/delete";
        }
        return "redirect:/admin/search";
    }

    @GetMapping("/{id}/view")
    public String view(@PathVariable long id, Model model) {
        Employee employee = employeeRepository.findById(id).get();
        model.addAttribute("employee", employee);
        return "admin/view";
    }

    @GetMapping("/{id}/delete")
    public String delete(@PathVariable long id) {
        employeeRepository.deleteById(id);
        return "redirect:/index";
    }

    @GetMapping("/report")
    public String showForm() {
        return "admin/report";
    }

    @PostMapping("/report")
    public ModelAndView searchEmployee(HttpServletRequest request) throws Exception {
        ModelAndView modelAndView = new ModelAndView("admin/showReport");
        User user = userRepository.findById(Long.parseLong(request.getParameter("id"))).get();
        DateFormat format = new SimpleDateFormat("yyyy-M-dd");
        Date from = format.parse(request.getParameter("from"));
        Date to = format.parse(request.getParameter("to"));

        List<Log> report = logRepository.findByUserAndTimestampBetween(user, from, to);
        modelAndView.addObject("report", report);
        return modelAndView;
    }

    @GetMapping("/showReport")
    public String showReport() {
        return "admin/showReport";
    }

}

