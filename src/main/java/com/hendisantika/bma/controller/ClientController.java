package com.hendisantika.bma.controller;

import com.hendisantika.bma.entity.Client;
import com.hendisantika.bma.entity.Log;
import com.hendisantika.bma.entity.User;
import com.hendisantika.bma.repository.ClientRepository;
import com.hendisantika.bma.repository.LogRepository;
import com.hendisantika.bma.repository.UserRepository;
import com.hendisantika.bma.validator.ClientValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bank-management-application
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-20
 * Time: 08:14
 */
@Controller
@RequestMapping("/clientOp")
public class ClientController {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private UserRepository userRepository;

    private ClientValidator validator;


    @GetMapping("/new")
    public String newClient() {
        return "clientOp/new";
    }

    @PostMapping("/new")
    public String addClient(HttpServletRequest request) {
        Client client = new Client();
        client.setName(request.getParameter("name"));
        client.setAddress(request.getParameter("address"));
        client.setEmail(request.getParameter("email"));
        client.setCnp(request.getParameter("CNP"));
        validator = new ClientValidator();
        if (validator.validate(client))
            clientRepository.save(client);
        else
            return "redirect:/clientOp/new?error=true";

        Log log = new Log();
        log.setOperation("Client added. ID: " + client.getId() + "; Name: " + client.getName());
        log.setTimestamp(new Timestamp(System.currentTimeMillis()));

        String username = request.getRemoteUser();

        User user = userRepository.findByUsername(username);
        log.setUser(user);

        logRepository.save(log);
        return "redirect:/index";
    }

    @GetMapping("/{id}/edit")
    public String update(@PathVariable long id, Model model) {
        Client client = clientRepository.findById(id).get();
        model.addAttribute("client", client);
        return "clientOp/edit";
    }

    @PostMapping("/update")
    public String updateClient(HttpServletRequest request) {
        Client client = clientRepository.findById(Long.parseLong(request.getParameter("id"))).get();
        client.setName(request.getParameter("name"));
        client.setAddress(request.getParameter("address"));
        client.setEmail(request.getParameter("email"));
        client.setCnp(request.getParameter("CNP"));
        validator = new ClientValidator();
        if (validator.validate(client))
            clientRepository.save(client);
        else
            return "redirect:/clientOp/new?error=true";
        clientRepository.save(client);

        Log log = new Log();

        log.setOperation("Client updated. ID: " + client.getId() + "; Name: " + client.getName());
        log.setTimestamp(new Timestamp(System.currentTimeMillis()));

        String username = request.getRemoteUser();

        User user = userRepository.findByUsername(username);
        log.setUser(user);
        logRepository.save(log);
        return "redirect:/index";
    }

    @GetMapping("/{id}/view")
    public String view(@PathVariable long id, Model model) {
        Client client = clientRepository.findById(id).get();
        model.addAttribute("client", client);
        return "clientOp/view";
    }

    @GetMapping("/search")
    public String displaySearch() {
        return "clientOp/search";
    }

    @PostMapping("/search")
    public String search(HttpServletRequest request) {
        Client client = clientRepository.findById(Long.parseLong(request.getParameter("id"))).get();
        String option = request.getParameter("option");
        if (option.equals("VIEW")) {
            return "redirect:/clientOp/" + client.getId() + "/view";
        }
        if (option.equals("EDIT")) {
            return "redirect:/clientOp/" + client.getId() + "/edit";
        }
        return "redirect:/clientOp/search";
    }
}
