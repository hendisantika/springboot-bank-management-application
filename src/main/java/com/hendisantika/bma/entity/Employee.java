package com.hendisantika.bma.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bank-management-application
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-03
 * Time: 13:06
 */
@Entity
@Data
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;

    private Double salary;

    private String email;

    private String phone;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User username;
}
