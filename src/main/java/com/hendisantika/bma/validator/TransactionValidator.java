package com.hendisantika.bma.validator;

import com.hendisantika.bma.entity.Account;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bank-management-application
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-03
 * Time: 18:20
 */
public class TransactionValidator {
    public boolean validate(Account from, Double amount) {
        return (from.getBalance() - amount) > 0 && amount > 0;
    }
}
