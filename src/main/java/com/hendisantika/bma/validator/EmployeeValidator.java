package com.hendisantika.bma.validator;

import com.hendisantika.bma.entity.Employee;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bank-management-application
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-03
 * Time: 18:19
 */
public class EmployeeValidator {
    public boolean validate(Employee employee) {
        return validateEmail(employee.getEmail()) && validateSalary(employee.getSalary());
    }

    private boolean validateName(String name) {
        return name.matches("^[a-zA-Z\\\\s]*$");
    }

    private boolean validateSalary(Double salary) {
        return salary > 0;
    }

    private boolean validateEmail(String email) {
        return email.contains("@") && email.contains(".com");
    }

    private boolean validatePhone(String phone) {
        return phone.matches("[07][0-9]{9}");
    }
}
