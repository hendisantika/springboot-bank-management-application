package com.hendisantika.bma.validator;

import com.hendisantika.bma.entity.Client;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bank-management-application
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-03
 * Time: 18:19
 */
public class ClientValidator {
    public boolean validate(Client client) {
        return validateCNP(client.getCnp()) && validateEmail(client.getEmail()) && validateName(client.getName());
    }

    private boolean validateName(String name) {
        return name.matches("[a-zA-Z\\s]");
    }

    private boolean validateCNP(String cnp) {
        return (cnp.startsWith("1") || cnp.startsWith("2")) && (cnp.length() == 13);
    }

    private boolean validateEmail(String email) {
        return email.contains("@") && email.contains(".com");
    }
}
