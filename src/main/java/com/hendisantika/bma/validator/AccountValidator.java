package com.hendisantika.bma.validator;

import com.hendisantika.bma.entity.Account;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bank-management-application
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-03
 * Time: 18:18
 */
public class AccountValidator {
    public boolean validate(Account account) {
        return (validateBalance(account.getBalance()) && validateType(account.getType()));
    }

    private boolean validateType(String type) {
        return type.equalsIgnoreCase("debit") || type.equalsIgnoreCase("credit");
    }

    private boolean validateBalance(Double balance) {
        return balance > 0;
    }
}
